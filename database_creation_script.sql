-- ### Tabelas ###

CREATE TABLE CF_PROFILE
(
CF_PROFILE_ID int NOT NULL AUTO_INCREMENT,
CF_PROFILE_DESCRIPTION varchar(255)
);

CREATE TABLE CF_USER
(
CF_USER_ID int NOT NULL AUTO_INCREMENT,
CF_PROFILE_ID int NOT NULL,
CF_USER_FIRSTNAME varchar(255),
CF_USER_LASTNAME varchar(255),
CF_USER_DISPLAYNAME varchar(255),
CF_USER_LOGIN varchar(255),
CF_USER_PASSWORD varchar(255)
);

CREATE TABLE CF_COUNTRY
(
CF_COUNTRY_ID int NOT NULL AUTO_INCREMENT,
CF_COUNTRY_NAME varchar(255)
);

CREATE TABLE CF_CITY
(
CF_CITY_ID int NOT NULL AUTO_INCREMENT,
CF_COUNTRY_ID int NOT NULL,
CF_CITY_NAME varchar(255)
);

CREATE TABLE CF_TRIP
(
CF_TRIP_ID int NOT NULL AUTO_INCREMENT,
CF_USER_ID int NOT NULL,
CF_COUNTRY_ID int NOT NULL,
CF_CITY_ID int,
CF_TRIP_TITLE varchar(255),
CF_TRIP_DESCRIPTION varchar(255),
CF_TRIP_VALUE decimal(12,2)
);

CREATE TABLE CF_BACK
(
CF_BACK_ID int NOT NULL AUTO_INCREMENT,
CF_TRIP_ID int NOT NULL,
CF_USER_ID int NOT NULL,
CF_BACK_COMMENT varchar(255),
CF_BACK_VALUE decimal(12,2)
);

-- ### Comentários ###

COMMENT ON COLUMN CF_TRIP.CF_USER_ID 
   IS 'USUÁRIO CRIADOR DA VIAGEM';

COMMENT ON COLUMN CF_BACK.CF_TRIP_ID 
   IS 'VIAGEM QUE ESTA SENDO APOIADA';

COMMENT ON COLUMN CF_BACK.CF_USER_ID 
   IS 'USUÁRIO CRIADOR DO APOIO';

-- ### CONTRAINTS ###

ALTER TABLE CF_USER
ADD CONSTRAINT FK_USERPROFILE
FOREIGN KEY (CF_PROFILE_ID)
REFERENCES CF_PROFILE(CF_PROFILE_ID);

ALTER TABLE CF_CITY
ADD CONSTRAINT FK_CITYCOUNTRY
FOREIGN KEY (CF_COUNTRY_ID)
REFERENCES CF_COUNTRY(CF_COUNTRY_ID);

ALTER TABLE CF_TRIP
ADD CONSTRAINT FK_TRIPUSER
FOREIGN KEY (CF_USER_ID)
REFERENCES CF_USER(CF_USER_ID);

ALTER TABLE CF_TRIP
ADD CONSTRAINT FK_TRIPCOUNTRY
FOREIGN KEY (CF_COUNTRY_ID)
REFERENCES CF_COUNTRY(CF_COUNTRY_ID);

ALTER TABLE CF_TRIP
ADD CONSTRAINT FK_TRIPCITY
FOREIGN KEY (CF_CITY_ID)
REFERENCES CF_CITY(CF_CITY_ID);

ALTER TABLE CF_BACK
ADD CONSTRAINT FK_BACKTRIP
FOREIGN KEY (CF_TRIP_ID)
REFERENCES CF_TRIP(CF_TRIP_ID);

ALTER TABLE CF_BACK
ADD CONSTRAINT FK_BACKUSER
FOREIGN KEY (CF_USER_ID)
REFERENCES CF_USER(CF_USER_ID);

-- ### Dados Inicializados ###

INSERT INTO CF_PROFILE (CF_PROFILE_DESCRIPTION)
VALUES ('ADMINISTRADOR'),
('USUARIO');

INSERT INTO CF_USER (CF_PROFILE_ID, CF_USER_FIRSTNAME, CF_USER_LASTNAME, CF_USER_DISPLAYNAME, CF_USER_LOGIN, CF_USER_PASSWORD)
VALUES (1, 'ADMINISTRADOR', '', 'ADMINISTRADOR', 'ADMIN', 'ADMIN123')

INSERT INTO CF_COUNTRY (CF_COUNTRY_NAME)
VALUES ('Afeganistão'),
('África do Sul'),
('Akrotiri'),
('Albânia'),
('Alemanha'),
('Andorra'),
('Angola'),
('Anguila'),
('Antárctida'),
('Antígua e Barbuda'),
('Antilhas Neerlandesas'),
('Arábia Saudita'),
('Arctic Ocean'),
('Argélia'),
('Argentina'),
('Arménia'),
('Aruba'),
('Ashmore and Cartier Islands'),
('Atlantic Ocean'),
('Austrália'),
('Áustria'),
('Azerbaijão'),
('Baamas'),
('Bangladeche'),
('Barbados'),
('Barém'),
('Bélgica'),
('Belize'),
('Benim'),
('Bermudas'),
('Bielorrússia'),
('Birmânia'),
('Bolívia'),
('Bósnia e Herzegovina'),
('Botsuana'),
('Brasil'),
('Brunei'),
('Bulgária'),
('Burquina Faso'),
('Burúndi'),
('Butão'),
('Cabo Verde'),
('Camarões'),
('Camboja'),
('Canadá'),
('Catar'),
('Cazaquistão'),
('Chade'),
('Chile'),
('China'),
('Chipre'),
('Clipperton Island'),
('Colômbia'),
('Comores'),
('Congo-Brazzaville'),
('Congo-Kinshasa'),
('Coral Sea Islands'),
('Coreia do Norte'),
('Coreia do Sul'),
('Costa do Marfim'),
('Costa Rica'),
('Croácia'),
('Cuba'),
('Dhekelia'),
('Dinamarca'),
('Domínica'),
('Egipto'),
('Emirados Árabes Unidos'),
('Equador'),
('Eritreia'),
('Eslováquia'),
('Eslovénia'),
('Espanha'),
('Estados Unidos'),
('Estónia'),
('Etiópia'),
('Faroé'),
('Fiji'),
('Filipinas'),
('Finlândia'),
('França'),
('Gabão'),
('Gâmbia'),
('Gana'),
('Gaza Strip'),
('Geórgia'),
('Geórgia do Sul e Sandwich do Sul'),
('Gibraltar'),
('Granada'),
('Grécia'),
('Gronelândia'),
('Guame'),
('Guatemala'),
('Guernsey'),
('Guiana'),
('Guiné'),
('Guiné Equatorial'),
('Guiné-Bissau'),
('Haiti'),
('Honduras'),
('Hong Kong'),
('Hungria'),
('Iémen'),
('Ilha Bouvet'),
('Ilha do Natal'),
('Ilha Norfolk'),
('Ilhas Caimão'),
('Ilhas Cook'),
('Ilhas dos Cocos'),
('Ilhas Falkland'),
('Ilhas Heard e McDonald'),
('Ilhas Marshall'),
('Ilhas Salomão'),
('Ilhas Turcas e Caicos'),
('Ilhas Virgens Americanas'),
('Ilhas Virgens Britânicas'),
('Índia'),
('Indian Ocean'),
('Indonésia'),
('Irão'),
('Iraque'),
('Irlanda'),
('Islândia'),
('Israel'),
('Itália'),
('Jamaica'),
('Jan Mayen'),
('Japão'),
('Jersey'),
('Jibuti'),
('Jordânia'),
('Kuwait'),
('Laos'),
('Lesoto'),
('Letónia'),
('Líbano'),
('Libéria'),
('Líbia'),
('Listenstaine'),
('Lituânia'),
('Luxemburgo'),
('Macau'),
('Macedónia'),
('Madagáscar'),
('Malásia'),
('Malávi'),
('Maldivas'),
('Mali'),
('Malta'),
('Isle of Man'),
('Marianas do Norte'),
('Marrocos'),
('Maurícia'),
('Mauritânia'),
('Mayotte'),
('México'),
('Micronésia'),
('Moçambique'),
('Moldávia'),
('Mónaco'),
('Mongólia'),
('Monserrate'),
('Montenegro'),
('Mundo'),
('Namíbia'),
('Nauru'),
('Navassa Island'),
('Nepal'),
('Nicarágua'),
('Níger'),
('Nigéria'),
('Niue'),
('Noruega'),
('Nova Caledónia'),
('Nova Zelândia'),
('Omã'),
('Pacific Ocean'),
('Países Baixos'),
('Palau'),
('Panamá'),
('Papua-Nova Guiné'),
('Paquistão'),
('Paracel Islands'),
('Paraguai'),
('Peru'),
('Pitcairn'),
('Polinésia Francesa'),
('Polonia'),
('Porto Rico'),
('Portugal'),
('Quenia'),
('Quirguizistão'),
('Quiribáti'),
('Reino Unido'),
('República Centro-Africana'),
('República Checa'),
('República Dominicana'),
('Roménia'),
('Ruanda'),
('Rússia'),
('Salvador'),
('Samoa'),
('Samoa Americana'),
('Santa Helena'),
('Santa Lúcia'),
('São Cristóvão e Neves'),
('São Marinho'),
('São Pedro e Miquelon'),
('São Tomé e Príncipe'),
('São Vicente e Granadinas'),
('Sara Ocidental'),
('Seicheles'),
('Senegal'),
('Serra Leoa'),
('Sérvia'),
('Singapura'),
('Síria'),
('Somália'),
('Southern Ocean'),
('Spratly Islands'),
('Sri Lanca'),
('Suazilândia'),
('Sudão'),
('Suécia'),
('Suíça'),
('Suriname'),
('Svalbard e Jan Mayen'),
('Tailândia'),
('Taiwan'),
('Tajiquistão'),
('Tanzânia'),
('Território Britânico do Oceano Índico'),
('Territórios Austrais Franceses'),
('Timor Leste'),
('Togo'),
('Tokelau'),
('Tonga'),
('Trindade e Tobago'),
('Tunísia'),
('Turquemenistão'),
('Turquia'),
('Tuvalu'),
('Ucrânia'),
('Uganda'),
('União Europeia'),
('Uruguai'),
('Usbequistão'),
('Vanuatu'),
('Vaticano'),
('Venezuela'),
('Vietname'),
('Wake Island'),
('Wallis e Futuna'),
('West Bank'),
('Zâmbia'),
('Zimbabué');
