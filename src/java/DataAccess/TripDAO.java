/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import DataAccess.Interface.iTripDAO;
import Models.Trip;
import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tiago
 */
public class TripDAO implements iTripDAO {
    @Override
    public void post(Trip b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        if (b.getId() == null) {
            em.persist(b);
        } else {
            em.merge(b);
        }
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void delete(Trip b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();        
        em.remove(em.merge(b));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Trip getById(Long id) {
        EntityManager em = JpaUtil.getEntityManager();
        Trip trip = em.find(Trip.class, id);
        em.close();
        return (trip);
    }

    @Override
    public List<Trip> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        List<Trip> listaTrips = em.createQuery("SELECT c FROM Trip c").getResultList();
        em.close();
        return (listaTrips);
    }
}
