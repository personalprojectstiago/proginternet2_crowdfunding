/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import DataAccess.Interface.iUserDAO;
import Models.TripUser;
import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Tiago
 */
public class UserDAO implements iUserDAO {
    @Override
    public void post(TripUser b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        if (b.getId() == null) {
            em.persist(b);
        } else {
            em.merge(b);
        }
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(TripUser b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();        
        em.remove(em.merge(b));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public TripUser getById(Long id) {
        EntityManager em = JpaUtil.getEntityManager();
        TripUser user = em.find(TripUser.class, id);
        em.close();
        return (user);
    }

    @Override
    public List<TripUser> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        List<TripUser> listaUsers = em.createQuery("SELECT t FROM TripUser t").getResultList();
        em.close();
        return (listaUsers);
    }

    @Override
    public TripUser getByUserId(long userId) {
        EntityManager em = JpaUtil.getEntityManager();
        Query query = em.createQuery("SELECT t FROM TripUser t"
                        + "WHERE t.userId = :userId ");
        query.setParameter("userId", "%" + userId + "%");        
        List<TripUser> listUser = query.getResultList();
        em.close();
        return (listUser.get(0));
    }
}
