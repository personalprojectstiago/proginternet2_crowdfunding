/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.Interface;

import Models.Trip;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface iTripDAO {
    public void delete(Trip t);
    public void post(Trip t);
    public Trip getById(Long id);
    public List<Trip> getAll();    
}
