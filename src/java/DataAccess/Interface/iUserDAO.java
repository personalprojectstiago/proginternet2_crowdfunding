/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.Interface;

import Models.TripUser;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface iUserDAO {
    public void delete(TripUser u);
    public void post(TripUser u);
    public TripUser getById(Long id);
    public List<TripUser> getAll();    
    public TripUser getByUserId(long userId);
}
