/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.Interface;

import Models.Back;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface iBackDAO {
    public void delete(Back b);
    public void post(Back b);
    public Back getById(Long id);
    public List<Back> getAll();    
}
