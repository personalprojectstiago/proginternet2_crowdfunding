/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.Interface;

import Models.City;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface iCityDAO {
    public void delete(City c);
    public void post(City c);
    public City getById(Long id);
    public List<City> getAll();    
}
