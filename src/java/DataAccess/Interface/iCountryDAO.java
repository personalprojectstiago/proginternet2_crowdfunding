/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.Interface;

import Models.Country;
import java.util.List;

/**
 *
 * @author Tiago
 */
public interface iCountryDAO {
    public void delete(Country c);
    public void post(Country c);
    public Country getById(Long id);
    public List<Country> getAll();    
}
