/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import DataAccess.Interface.iCityDAO;
import Models.City;
import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tiago
 */
public class CityDAO implements iCityDAO {
    @Override
    public void post(City c) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        if (c.getId() == null) {
            em.persist(c);
        } else {
            em.merge(c);
        }
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void delete(City c) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();        
        em.remove(em.merge(c));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public City getById(Long id) {
        EntityManager em = JpaUtil.getEntityManager();
        City city = em.find(City.class, id);
        em.close();
        return (city);
    }

    @Override
    public List<City> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        List<City> listaCitys = em.createQuery("SELECT c FROM City c").getResultList();
        em.close();
        return (listaCitys);
    }
}
