/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import DataAccess.Interface.iCountryDAO;
import Models.Country;
import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tiago
 */
public class CountryDAO implements iCountryDAO {
    @Override
    public void post(Country b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        if (b.getId() == null) {
            em.persist(b);
        } else {
            em.merge(b);
        }
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void delete(Country b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();        
        em.remove(em.merge(b));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Country getById(Long id) {
        EntityManager em = JpaUtil.getEntityManager();
        Country country = em.find(Country.class, id);
        em.close();
        return (country);
    }

    @Override
    public List<Country> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        List<Country> listaCountrys = em.createQuery("SELECT c FROM Country c").getResultList();
        em.close();
        return (listaCountrys);
    }
}
