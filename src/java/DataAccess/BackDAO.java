/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import DataAccess.Interface.iBackDAO;
import Models.Back;
import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tiago
 */
public class BackDAO implements iBackDAO {
    @Override
    public void post(Back b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        if (b.getId() == null) {
            em.persist(b);
        } else {
            em.merge(b);
        }
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void delete(Back b) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();        
        em.remove(em.merge(b));
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Back getById(Long id) {
        EntityManager em = JpaUtil.getEntityManager();
        Back back = em.find(Back.class, id);
        em.close();
        return (back);
    }

    @Override
    public List<Back> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        List<Back> listaBacks = em.createQuery("SELECT c FROM Back c").getResultList();
        em.close();
        return (listaBacks);
    }
}
