package Filters;

import Controllers.LoginBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tiago
 */
@WebFilter(filterName = "AccessFilter", urlPatterns = {"/faces/admin/"})
public class AccessFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) 
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("loginBean");
        if (loginBean != null && loginBean.verifyIfIsLogged() && loginBean.verifyIfIsAdmin()) {
            chain.doFilter(request, response);
        } else {            
            resp.sendRedirect(req.getContextPath() + "/faces/index.xhtml");
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

}
