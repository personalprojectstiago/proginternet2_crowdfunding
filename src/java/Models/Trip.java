/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.apache.jasper.tagplugins.jstl.ForEach;

/**
 *
 * @author Tiago
 */
@Entity
public class Trip implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Country country;
    @ManyToOne
    private City city;
    @ManyToOne
    private TripUser user;
    @OneToMany(cascade={CascadeType.ALL})
    private List<Back> backs;
    private Boolean isEnabled;
    
    public Trip(){        
    }
    
    public Trip(Country country, City city, TripUser user, List<Back> backs, Boolean isEnabled){
        this.country = country;
        this.city = city;
        this.user = user;
        this.backs = backs;
        this.isEnabled = isEnabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public TripUser getUser() {
        return user;
    }    

    public List<Back> getBacks() {
        return backs;
    }

    public void setBacks(List<Back> backs) {
        this.backs = backs;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
        
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trip)) {
            return false;
        }
        Trip other = (Trip)object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Trip[ id=" + id + " ]";
    } 
}
