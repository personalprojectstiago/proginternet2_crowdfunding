package LoginAPI;

import Models.LoginAPI.UserRest;
import com.google.gson.Gson;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class LoginRestClient {
    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/LoginRest/webresources";

    public LoginRestClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("login");
    }

    public <T> T getAllUsers(GenericType<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(MediaType.APPLICATION_JSON).get(responseType);
    }

    public Long PostUser(UserRest user) throws ClientErrorException {
        return webTarget.request(MediaType.TEXT_PLAIN).
                post(Entity.entity(user, MediaType.APPLICATION_JSON), Long.class);
    }

    public UserRest validateUser(UserRest user) throws ClientErrorException {
        WebTarget resource = webTarget;
        Gson gson = new Gson();
        String json = gson.toJson(user);
        System.out.println(json);

        Response response =  resource.request().post(Entity.entity(json, MediaType.APPLICATION_JSON));
        System.out.println(response.getStatus());
        
        final String responseEntity = response.readEntity(String.class);        
        return(gson.fromJson(responseEntity, UserRest.class));
    }

    public void close() {
        client.close();
    }
    
}
