package Util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtil {
    private static EntityManagerFactory emf;
    
    public static EntityManager getEntityManager() {
        emf = Persistence.createEntityManagerFactory("CrowdfundingProjectPU");
        return emf.createEntityManager();
    }
    
    public static void close(){
        emf.close();
    }
    
}
