/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DataAccess.UserDAO;
import Helpers.EnumTypes;
import LoginAPI.LoginRestClient;
import Models.LoginAPI.UserRest;
import Models.TripUser;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Tiago
 */
@ManagedBean
@SessionScoped
public class LoginBean {
    private TripUser loggedUser;
    private String login;
    private String password;
    private boolean isLogged;
    
    private List<TripUser> users;

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
        //ApplicationBean appBean = (ApplicationBean)FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("applicationBean");
        //this.users = appBean.getUsers();
    }

    public TripUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(TripUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public boolean verifyIfIsLogged(){
        return this.isLogged;
    }
    
    public boolean verifyIfIsAdmin(){
        if (this.loggedUser != null && this.loggedUser.getProfile().equals(EnumTypes.Profile.Administrator))
            return true;
        return false;
    }
    
    public String validateLogin(){        
        isLogged = false;
        
        LoginRestClient client = new LoginRestClient();
        UserRest user = client.validateUser(new UserRest(this.login, this.password));
               
        if (user.isValid()){
            isLogged = true;
            UserDAO userDAO = new UserDAO();
            this.loggedUser = userDAO.getByUserId(user.getId());
            this.loggedUser.setUser(user);
        }
        if (isLogged){
            if (this.loggedUser.getProfile().equals(EnumTypes.Profile.Administrator))
                return "admin/home";
            else
                return "user/home";
        }   
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Login inválido!", "Usuário ou senha estão errados!");
        context.addMessage("loginMessage", mensagem);
        return ("/login");
    }

    public String logout() {
        this.loggedUser = null;
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return ("/login?faces-redirect=true");
    }    
    
}
