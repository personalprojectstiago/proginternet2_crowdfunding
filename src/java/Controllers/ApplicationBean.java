/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Helpers.EnumTypes;
import Models.Back;
import Models.City;
import Models.Country;
import Models.Trip;
import Models.TripUser;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Tiago
 */
@ManagedBean (eager = true)
@SessionScoped
public class ApplicationBean {

    List<TripUser> users;
    List<Trip> trips;
    /**
     * Creates a new instance of ApplicationBean
     */
    public ApplicationBean() {
        this.populateLists();
    }
    
    private void populateLists(){
        this.users = new ArrayList<TripUser>();
        
        TripUser admin = new TripUser(1, "admin", "admin123", EnumTypes.Profile.Administrator);
//        TripUser pedro = new TripUser(2, "Pedro", "user123", EnumTypes.Profile.User);
//        TripUser joao = new TripUser(3, "João", "user123", EnumTypes.Profile.User);
//        TripUser maria = new TripUser(4, "Maria", "user123", EnumTypes.Profile.User);
//        TripUser paulo = new TripUser(5, "Paulo", "user123", EnumTypes.Profile.User);
//        TripUser tiago = new TripUser(6, "Tiago", "user123", EnumTypes.Profile.User);
        
        this.users.add(admin);
//        this.users.add(pedro);
//        this.users.add(joao);
//        this.users.add(maria);
//        this.users.add(tiago);
//        
//        List<Back> backs1 = new ArrayList<Back>();
//        backs1.add(new Back(joao, 150.00));
//        backs1.add(new Back(pedro, 60.00));
//        backs1.add(new Back(paulo, 80.00));
//        backs1.add(new Back(tiago, 300.00));
//        
//        List<Back> backs2 = new ArrayList<Back>();
//        backs2.add(new Back(joao, 30.00));
//        backs2.add(new Back(maria, 100.00));
//        backs2.add(new Back(paulo, 180.00));
//        backs2.add(new Back(pedro, 500.00));
//        
//        List<Back> backs3 = new ArrayList<Back>();
//        backs3.add(new Back(joao, 10.00));
//        backs3.add(new Back(maria, 150.00));
//        backs3.add(new Back(paulo, 90.00));
//        backs3.add(new Back(tiago, 1800.00));
//        
//        List<Back> backs4 = new ArrayList<Back>();
//        backs4.add(new Back(pedro, 60.00));
//        backs4.add(new Back(maria, 120.00));
//        backs4.add(new Back(paulo, 250.00));
//        backs4.add(new Back(tiago, 4500.00));
//        
//        this.trips = new ArrayList<Trip>();
//        this.trips.add(new Trip(new Country("Brasil"), new City("Buzios"), maria, backs1, true));
//        this.trips.add(new Trip(new Country("Estados Unidos"), new City("Nova York"), tiago, backs2, true));
//        this.trips.add(new Trip(new Country("Austrália"), new City("Sidney"), pedro, backs3, true));
//        this.trips.add(new Trip(new Country("Inglaterra"), new City("Londres"), joao, backs4, true));
    }

    public List<TripUser> getUsers() {
        return users;
    }

    public void setUsers(List<TripUser> users) {
        this.users = users;
    }    

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
