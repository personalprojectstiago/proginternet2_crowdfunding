/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DataAccess.UserDAO;
import LoginAPI.LoginRestClient;
import Models.LoginAPI.UserRest;
import Models.TripUser;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author Tiago
 */
@ManagedBean
@RequestScoped
public class UserBean {

    private TripUser currentUser;
    private UserRest userRest;
     
    private boolean skip;

    public UserBean() {
        currentUser = new TripUser();
        userRest = new UserRest();
    }

    public TripUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(TripUser currentUser) {
        this.currentUser = currentUser;
    }

    public UserRest getUserRest() {
        return userRest;
    }

    public void setUserRest(UserRest userRest) {
        this.userRest = userRest;
    }

    public List<TripUser> getListaUsers() {
        return new UserDAO().getAll();
    }

    public String newUser() {
        currentUser = new TripUser();
        return ("adduser?faces-redirect=true");
    }

    public String addUser() {
        if (validateCurrentUser()){
            LoginRestClient loginClient = new LoginRestClient();
            currentUser.setUserId(loginClient.PostUser(this.userRest));            
            new UserDAO().post(currentUser);
            currentUser = new TripUser();
            return ("index?faces-redirect=true");
        }
        else{
            //currentUser.setPassword("");
            return ("adduser?faces-redirect=true");
        }
    }

    public String editUser(TripUser u) {
        currentUser = u;
        return ("edituser?faces-redirect=true");
    }

    public String updateUser() {
        new UserDAO().post(currentUser);
        return ("login?faces-redirect=true");
    }
    
    public void removerUser(TripUser u) {
        new UserDAO().delete(u);
    }    
     
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }    

    private boolean validateCurrentUser() {
        boolean isValid = true;        
        
        String name = this.currentUser.getFirstName();
        String lastName = this.currentUser.getLastName();
        String userName = this.userRest.getUserName();
        String password = this.userRest.getPassword();
        String email = this.currentUser.getEmail();
        
        if (name.isEmpty()){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Nome é obrigatório!");
            contexto.addMessage(null, mensagem);
            isValid = false;
        }else if (name.length() < 4){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Nome deve ter mais de três caracteres!");
            contexto.addMessage(null, mensagem);  
            isValid = false;          
        }
        
        if (lastName.isEmpty()){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Sobrenome é obrigatório!");
            contexto.addMessage(null, mensagem);
            isValid = false;
        }else if (lastName.length() < 4){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Sobrenome deve ter mais de três caracteres!");
            contexto.addMessage(null, mensagem);   
            isValid = false;         
        }
        
        if (userName.isEmpty()){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Usuário é obrigatório!");
            contexto.addMessage(null, mensagem);
            isValid = false;
        }else if (userName.length() < 4){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Usuário deve ter mais de três caracteres!");
            contexto.addMessage(null, mensagem);    
            isValid = false;        
        }
        
        if (password.isEmpty()){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Senha é obrigatória!");
            contexto.addMessage(null, mensagem);
            isValid = false;
        }else if (password.length() < 4){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Senha deve ter mais de três caracteres!");
            contexto.addMessage(null, mensagem);  
            isValid = false;          
        }
        
        if (email.isEmpty()){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Email é obrigatório!");
            contexto.addMessage(null, mensagem);
            isValid = false;
        }else if (email.length() < 4){
            FacesContext contexto = FacesContext.getCurrentInstance();
            FacesMessage mensagem = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Erro de entrada!",
                "Email deve ter mais de três caracteres!");
            contexto.addMessage(null, mensagem); 
            isValid = false;           
        }
        
        return isValid;
    }
}
