/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Back;
import Models.City;
import Models.Country;
import Models.Trip;
import Models.TripUser;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Tiago
 */
@ManagedBean
@SessionScoped
public class TripBean {
    private List<Trip> trips;
    private List<Trip> loggedUserTrips;
    private Trip selectedTrip;
    private double selectedTripAllBackValue;
    private List<String> countries;
    private Trip newTrip;
    private String newTripCountry;
    private String newTripCity;
    
    /**
     * Creates a new instance of TripBean
     */
    public TripBean() {
        ApplicationBean appBean = (ApplicationBean)FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("applicationBean");
        this.trips = appBean.getTrips();       
        
        LoginBean loginBean = (LoginBean)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean");
        this.mountLogedUserTrips(loginBean.getLoggedUser());
        this.mountCountryList();
    }
    
    private void mountLogedUserTrips(TripUser user){
        if (trips != null){
            this.loggedUserTrips = new ArrayList<Trip>();
            for (Trip trip : trips){
                if (trip.getUser().getId().equals(user.getId())){
                    this.loggedUserTrips.add(trip);
                }
            }
        }
    }
    
    private void mountCountryList(){
        this.countries = new ArrayList<String>();
        this.countries.add("Afeganistão");
        this.countries.add("África do Sul");
        this.countries.add("Akrotiri");
        this.countries.add("Albânia");
        this.countries.add("Alemanha");
        this.countries.add("Andorra");
        this.countries.add("Angola");
        this.countries.add("Anguila");
        this.countries.add("Antárctida");
        this.countries.add("Antígua e Barbuda");
        this.countries.add("Antilhas Neerlandesas");
        this.countries.add("Arábia Saudita");
        this.countries.add("Arctic Ocean");
        this.countries.add("Argélia");
        this.countries.add("Argentina");
        this.countries.add("Arménia");
        this.countries.add("Aruba");
        this.countries.add("Ashmore and Cartier Islands");
        this.countries.add("Atlantic Ocean");
        this.countries.add("Austrália");
        this.countries.add("Áustria");
        this.countries.add("Azerbaijão");
        this.countries.add("Baamas");
        this.countries.add("Bangladeche");
        this.countries.add("Barbados");
        this.countries.add("Barém");
        this.countries.add("Bélgica");
        this.countries.add("Belize");
        this.countries.add("Benim");
        this.countries.add("Bermudas");
        this.countries.add("Bielorrússia");
        this.countries.add("Birmânia");
        this.countries.add("Bolívia");
        this.countries.add("Bósnia e Herzegovina");
        this.countries.add("Botsuana");
        this.countries.add("Brasil");
        this.countries.add("Brunei");
        this.countries.add("Bulgária");
        this.countries.add("Burquina Faso");
        this.countries.add("Burúndi");
        this.countries.add("Butão");
        this.countries.add("Cabo Verde");
        this.countries.add("Camarões");
        this.countries.add("Camboja");
        this.countries.add("Canadá");
        this.countries.add("Catar");
        this.countries.add("Cazaquistão");
        this.countries.add("Chade");
        this.countries.add("Chile");
        this.countries.add("China");
        this.countries.add("Chipre");
        this.countries.add("Clipperton Island");
        this.countries.add("Colômbia");
        this.countries.add("Comores");
        this.countries.add("Congo-Brazzaville");
        this.countries.add("Congo-Kinshasa");
        this.countries.add("Coral Sea Islands");
        this.countries.add("Coreia do Norte");
        this.countries.add("Coreia do Sul");
        this.countries.add("Costa do Marfim");
        this.countries.add("Costa Rica");
        this.countries.add("Croácia");
        this.countries.add("Cuba");
        this.countries.add("Dhekelia");
        this.countries.add("Dinamarca");
        this.countries.add("Domínica");
        this.countries.add("Egipto");
        this.countries.add("Emirados Árabes Unidos");
        this.countries.add("Equador");
        this.countries.add("Eritreia");
        this.countries.add("Eslováquia");
        this.countries.add("Eslovénia");
        this.countries.add("Espanha");
        this.countries.add("Estados Unidos");
        this.countries.add("Estónia");
        this.countries.add("Etiópia");
        this.countries.add("Faroé");
        this.countries.add("Fiji");
        this.countries.add("Filipinas");
        this.countries.add("Finlândia");
        this.countries.add("França");
        this.countries.add("Gabão");
        this.countries.add("Gâmbia");
        this.countries.add("Gana");
        this.countries.add("Gaza Strip");
        this.countries.add("Geórgia");
        this.countries.add("Geórgia do Sul e Sandwich do Sul");
        this.countries.add("Gibraltar");
        this.countries.add("Granada");
        this.countries.add("Grécia");
        this.countries.add("Gronelândia");
        this.countries.add("Guame");
        this.countries.add("Guatemala");
        this.countries.add("Guernsey");
        this.countries.add("Guiana");
        this.countries.add("Guiné");
        this.countries.add("Guiné Equatorial");
        this.countries.add("Guiné-Bissau");
        this.countries.add("Haiti");
        this.countries.add("Honduras");
        this.countries.add("Hong Kong");
        this.countries.add("Hungria");
        this.countries.add("Iémen");
        this.countries.add("Ilha Bouvet");
        this.countries.add("Ilha do Natal");
        this.countries.add("Ilha Norfolk");
        this.countries.add("Ilhas Caimão");
        this.countries.add("Ilhas Cook");
        this.countries.add("Ilhas dos Cocos");
        this.countries.add("Ilhas Falkland");
        this.countries.add("Ilhas Heard e McDonald");
        this.countries.add("Ilhas Marshall");
        this.countries.add("Ilhas Salomão");
        this.countries.add("Ilhas Turcas e Caicos");
        this.countries.add("Ilhas Virgens Americanas");
        this.countries.add("Ilhas Virgens Britânicas");
        this.countries.add("Índia");
        this.countries.add("Indian Ocean");
        this.countries.add("Indonésia");
        this.countries.add("Irão");
        this.countries.add("Iraque");
        this.countries.add("Irlanda");
        this.countries.add("Islândia");
        this.countries.add("Israel");
        this.countries.add("Itália");
        this.countries.add("Jamaica");
        this.countries.add("Jan Mayen");
        this.countries.add("Japão");
        this.countries.add("Jersey");
        this.countries.add("Jibuti");
        this.countries.add("Jordânia");
        this.countries.add("Kuwait");
        this.countries.add("Laos");
        this.countries.add("Lesoto");
        this.countries.add("Letónia");
        this.countries.add("Líbano");
        this.countries.add("Libéria");
        this.countries.add("Líbia");
        this.countries.add("Listenstaine");
        this.countries.add("Lituânia");
        this.countries.add("Luxemburgo");
        this.countries.add("Macau");
        this.countries.add("Macedónia");
        this.countries.add("Madagáscar");
        this.countries.add("Malásia");
        this.countries.add("Malávi");
        this.countries.add("Maldivas");
        this.countries.add("Mali");
        this.countries.add("Malta");
        this.countries.add("Isle of Man");
        this.countries.add("Marianas do Norte");
        this.countries.add("Marrocos");
        this.countries.add("Maurícia");
        this.countries.add("Mauritânia");
        this.countries.add("Mayotte");
        this.countries.add("México");
        this.countries.add("Micronésia");
        this.countries.add("Moçambique");
        this.countries.add("Moldávia");
        this.countries.add("Mónaco");
        this.countries.add("Mongólia");
        this.countries.add("Monserrate");
        this.countries.add("Montenegro");
        this.countries.add("Mundo");
        this.countries.add("Namíbia");
        this.countries.add("Nauru");
        this.countries.add("Navassa Island");
        this.countries.add("Nepal");
        this.countries.add("Nicarágua");
        this.countries.add("Níger");
        this.countries.add("Nigéria");
        this.countries.add("Niue");
        this.countries.add("Noruega");
        this.countries.add("Nova Caledónia");
        this.countries.add("Nova Zelândia");
        this.countries.add("Omã");
        this.countries.add("Pacific Ocean");
        this.countries.add("Países Baixos");
        this.countries.add("Palau");
        this.countries.add("Panamá");
        this.countries.add("Papua-Nova Guiné");
        this.countries.add("Paquistão");
        this.countries.add("Paracel Islands");
        this.countries.add("Paraguai");
        this.countries.add("Peru");
        this.countries.add("Pitcairn");
        this.countries.add("Polinésia Francesa");
        this.countries.add("Polonia");
        this.countries.add("Porto Rico");
        this.countries.add("Portugal");
        this.countries.add("Quenia");
        this.countries.add("Quirguizistão");
        this.countries.add("Quiribáti");
        this.countries.add("Reino Unido");
        this.countries.add("República Centro-Africana");
        this.countries.add("República Checa");
        this.countries.add("República Dominicana");
        this.countries.add("Roménia");
        this.countries.add("Ruanda");
        this.countries.add("Rússia");
        this.countries.add("Salvador");
        this.countries.add("Samoa");
        this.countries.add("Samoa Americana");
        this.countries.add("Santa Helena");
        this.countries.add("Santa Lúcia");
        this.countries.add("São Cristóvão e Neves");
        this.countries.add("São Marinho");
        this.countries.add("São Pedro e Miquelon");
        this.countries.add("São Tomé e Príncipe");
        this.countries.add("São Vicente e Granadinas");
        this.countries.add("Sara Ocidental");
        this.countries.add("Seicheles");
        this.countries.add("Senegal");
        this.countries.add("Serra Leoa");
        this.countries.add("Sérvia");
        this.countries.add("Singapura");
        this.countries.add("Síria");
        this.countries.add("Somália");
        this.countries.add("Southern Ocean");
        this.countries.add("Spratly Islands");
        this.countries.add("Sri Lanca");
        this.countries.add("Suazilândia");
        this.countries.add("Sudão");
        this.countries.add("Suécia");
        this.countries.add("Suíça");
        this.countries.add("Suriname");
        this.countries.add("Svalbard e Jan Mayen");
        this.countries.add("Tailândia");
        this.countries.add("Taiwan");
        this.countries.add("Tajiquistão");
        this.countries.add("Tanzânia");
        this.countries.add("Território Britânico do Oceano Índico");
        this.countries.add("Territórios Austrais Franceses");
        this.countries.add("Timor Leste");
        this.countries.add("Togo");
        this.countries.add("Tokelau");
        this.countries.add("Tonga");
        this.countries.add("Trindade e Tobago");
        this.countries.add("Tunísia");
        this.countries.add("Turquemenistão");
        this.countries.add("Turquia");
        this.countries.add("Tuvalu");
        this.countries.add("Ucrânia");
        this.countries.add("Uganda");
        this.countries.add("União Europeia");
        this.countries.add("Uruguai");
        this.countries.add("Usbequistão");
        this.countries.add("Vanuatu");
        this.countries.add("Vaticano");
        this.countries.add("Venezuela");
        this.countries.add("Vietname");
        this.countries.add("Wake Island");
        this.countries.add("Wallis e Futuna");
        this.countries.add("West Bank");
        this.countries.add("Zâmbia");
        this.countries.add("Zimbabué");
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public List<Trip> getLoggedUserTrips() {
        return loggedUserTrips;
    }

    public void setLoggedUserTrips(List<Trip> logedUserTrips) {
        this.loggedUserTrips = logedUserTrips;
    }
    
    public Trip getSelectedTrip() {
        return selectedTrip;
    }

    public void setSelectedTrip(Trip selectedTrip) {
        this.selectedTrip = selectedTrip;
        this.selectedTripAllBackValue = calculateTripAllBackValue();
    }

    public double getSelectedTripAllBackValue() {
        return selectedTripAllBackValue;
    }
    
    public double calculateTripAllBackValue(){
        double total = 0;
        for (Back back : this.selectedTrip.getBacks()){
            total += back.getValue();
        }        
        return total;
    }    

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public Trip getNewTrip() {
        return newTrip;
    }

    public void setNewTrip(Trip newTrip) {
        this.newTrip = newTrip;
    }

    public String getNewTripCountry() {
        return newTripCountry;
    }

    public void setNewTripCountry(String newTripCountry) {
        this.newTripCountry = newTripCountry;
    }

    public String getNewTripCity() {
        return newTripCity;
    }

    public void setNewTripCity(String newTripCity) {
        this.newTripCity = newTripCity;
    }
    
    public String addNewTripToList(){
        LoginBean loginBean = (LoginBean)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean");        
        this.newTrip = new Trip(new Country(this.newTripCountry), new City(this.newTripCity), loginBean.getLoggedUser(), new ArrayList<Back>(), Boolean.TRUE);
        this.trips.add(newTrip);
        this.mountLogedUserTrips(loginBean.getLoggedUser());
        return "mytrips";
    }
    
    public void removeTripFromList(){
        this.trips.remove(this.selectedTrip);
        this.loggedUserTrips.remove(this.selectedTrip);
    }
}
